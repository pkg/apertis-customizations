# vim:syntax=apparmor
#
#    Copyright (C) 2002-2009 Novell/SUSE
#    Copyright (C) 2009-2011 Canonical Ltd.
#    Copyright (C) 2012-2015 Collabora Ltd.
#
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of version 2 of the GNU General Public
#    License published by the Free Software Foundation.
#
#    This package is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

###
# <abstractions/chaiwala-base>, based on <abstractions/base> in apparmor
#
# This file is the base abstraction for Apertis. In principle, generic GUI
# applications that have no particular special requirements should be able
# to use this abstraction as a base.
#
# FIXME: This abstraction needs auditing: it is a mixture of completely
# benign things, and things that should not necessarily be allowed.
# https://phabricator.apertis.org/T3588
#
# Status: Apertis-specific
# Privilege level: intended to be used by unprivileged apps
# Typical users: all app-bundles
# Dependencies: <tunables/global>
###

  #include <abstractions/base>

  # FIXME: this is full read/write access to all of dconf, and should be
  # locked down. See https://phabricator.apertis.org/T292
  #include <abstractions/gsettings>

  # Include the X abstraction so applications can use GL
  #include <abstractions/X>
  #include <abstractions/wayland>

  @{PROC}/cmdline                r,

  # D-Bus
  /etc/machine-id                r,

  # Apertis - User data policy: no @{HOME} or other user's data dir are given by default
  # FIXME: do we really want to deny this without auditing to the log?
  # https://phabricator.apertis.org/T3588
  deny @{HOMEDIRS}               rw,

  # Apertis Execution rules: nothing should be excuted unless:
  # - it has a profile
  # - it can be run into the chaiwala_sanitized_helper provided in
  # abstractions/chaiwala-helpers.
  # Under any circumstance chaiwala-base should contain generic rules like
  # /usr/bin/* Cx -> chaiwala_sanitized_helper
  # or
  # /usr/bin/* Px
  # since it will preclude other profile to provide a different generic rule if
  # they need (rules will conflict)

  # Almost any graphical application will need to be able to use EGL
  /usr/lib/@{multiarch}/egl/*.so rm,

  # Many tools use these, and this is about as restrictive as we can get
  # with only AppArmor; going further needs namespaces (T2006)
  @{PROC}/@{pid}/cmdline	r,
  owner @{PROC}/@{pid}/auxv	r,
  owner @{PROC}/@{pid}/fd/	r,
  owner @{PROC}/@{pid}/status	r,

  # Pango
  /etc/pango/*                    r,
  /usr/lib/@{multiarch}/pango/** mr,

  # Access to gvfs, bug 1510
  # FIXME: This should not be allowed: only processes that are allowed to
  # read all user files should be allowed to read their metadata.
  # https://phabricator.apertis.org/T3588
  @{HOME}/.local/share/gvfs-metadata/	r,
  @{HOME}/.local/share/gvfs-metadata/*	r,

  # No reason to limit access to /usr/share for applications
  /usr/share/   r,
  /usr/share/** r,

  /etc/ssl/certs/ca-certificates.crt r,
