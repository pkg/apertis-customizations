#!/bin/bash
set -ve

COMPONENTS="""
 hmi/libseaton
 hmi/libclapton
 hmi/libgrassmoor
 hmi/shapwick
 appfw/canterbury
 appfw/rhosydd
 appfw/traprain
 hmi/libthornbury
 hmi/liblightwood
 appfw/barkway
 appfw/beckfoot
 appfw/didcot
 hmi/mildenhall
 hmi/libshoreham
 hmi/mildenhall-launcher
 hmi/mildenhall-compositor
 hmi/eye
 appfw/frampton
 appfw/newport
 hmi/prestwood
 appfw/ribchester
 appfw/frome
 hmi/tinwell
 hmi/mildenhall-settings
 hmi/mildenhall-statusbar
 hmi/mildenhall-popup-layer
 hmi/libbredon
"""

installdir=$(pwd)/build
mkdir -p ${installdir}

export PKG_CONFIG_PATH=${installdir}/lib/pkgconfig
export XDG_DATA_DIRS=/usr/share:/usr/local/share:${installdir}/share

for x in ${COMPONENTS}; do
  echo ==== Building ${x} ===
  pushd ${x}
  if ! [ -e configure ]; then
    echo = autogen ${x} =
    rm -f Makefile
    NOCONFIGURE=1 ./autogen.sh
    if [ -e Makefile ]; then
      # Remove the bogus makefile & configure to force autoconf to happen again
      # next round
      rm -f configure Makefile
      echo "${x} didn't handle NOCONFIGURE"
      exit 1
    fi
  fi
  if ! [ -e Makefile ]; then
    echo = configure ${x} =
    ./configure --prefix ${installdir} \
      --with-systemdunitdir=${installdir}/lib/systemd/user
  fi
  echo = build ${x} =
  make -j4
  echo = install ${x} =
  make -j4 install
  echo "=== ${x} DONE ==="
  popd
done

